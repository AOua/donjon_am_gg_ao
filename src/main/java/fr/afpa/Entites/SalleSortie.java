package fr.afpa.Entites;

import java.util.Arrays;

public class SalleSortie extends Salle{

	
	public SalleSortie() {
		super();
	}
	
	

	@Override
	public String toString() {
		return "SalleSortie [compteurMonstre=" + compteurMonstre + ", configSalle=" + configSalle + ", numero=" + numero
				+ ", porte=" + Arrays.toString(porte) + "]";
	}

	
	
}
