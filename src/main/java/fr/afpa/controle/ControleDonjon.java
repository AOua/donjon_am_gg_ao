package fr.afpa.controle;

import fr.afpa.Entites.Coordonnee;
import fr.afpa.Entites.Donjon;
import fr.afpa.Entites.Portes;

public class ControleDonjon {

	/**
	 * fonction qui control si la porte ne s'ouvre pas sur l'exterieur du plateau
	 * 
	 * @param donjon       le donjon initial
	 * @param numPorte     le numero de la porte a ouvrire
	 * @param xSalleCibler l'abscisse de la salle dans le plateau
	 * @param ySalleCibler l'ordonnée de la salle dans le plateau
	 * @return une porte qui existe
	 */
	public static boolean controlPorteInterneDonjon(Donjon donjon, int numPorte, int xSalleCibler, int ySalleCibler) {

		if (numPorte == 0 && xSalleCibler == 0) {
			return true;
		} else if (numPorte == 1 && ySalleCibler == donjon.getDimension() - 1) {
			return true;
		} else if (numPorte == 2 && xSalleCibler == donjon.getDimension() - 1) {
			return true;
		} else if (numPorte == 3 && ySalleCibler == 0) {
			return true;
		}
		return false;
	}

	/**
	 * fonction qui verifie si la salle selectionnee a la meme valeur
	 * 
	 * @param valeurCelulle tableau de valeur
	 * @param xCellule1
	 * @param yCellule1
	 * @param xCellule2
	 * @param yCellule2
	 * @return
	 */
	public static boolean verifValeurCellule(int[][] valeurCelulle, int xCellule1, int yCellule1, int xCellule2,
			int yCellule2) {
		if (valeurCelulle[xCellule1][yCellule1] == valeurCelulle[xCellule2][xCellule2]) {
			return true;
		}
		return false;
	}

	/**
	 * fonction qui verifie si la valeur de la salle cible est identique a celle de
	 * la salle de l'autre cote de la porte
	 * 
	 * @param valeurCellule               tableau de valeur des salles
	 * @param xCible                      x de la salle cible
	 * @param yCible                      y de la salle cible
	 * @param coordonneeSalleDeLAutreCote coordonnee de la salle de l'autre cote de
	 *                                    la porte
	 * @return un boolean pour la verification
	 */
	public static boolean verifValeurSalle(int[][] valeurCellule, int xCible, int yCible,
			Coordonnee coordonneeSalleDeLAutreCote) {
		if (valeurCellule[xCible][yCible] == valeurCellule[coordonneeSalleDeLAutreCote
				.getX()][coordonneeSalleDeLAutreCote.getY()]) {
			return true;
		}
		return false;
	}

	/**
	 * Fonction qui verifie si les pieces n'ont pas plus de 2 porte ouvertes
	 * 
	 * @param donjon le donjon initiale
	 * @param xCible coordonnée x de la salle cible
	 * @param yCible coordeonnée y de la salle cible
	 * @return un boolean pour la verfication
	 */
	public static boolean verifMax2PortesOuverte(Donjon donjon, int xCible, int yCible) {
		int nbPorteOuverte = 0;
		for (int i = 0; i < donjon.getPlateau()[xCible][yCible].getPorte().length; i++) {
			if (donjon.getPlateau()[xCible][yCible].getPorte()[i].isPassage() == true) {
				nbPorteOuverte++;
			}
			if (nbPorteOuverte > 3) {
				return true;
			}
		}

		return false;
	}
	/**
	 * Fonction qui verifie que toutes les valeur du tableau sont identique
	 * @param valeur	le tableau de valeur
	 * @return	boolean pour la verification
	 */
	public static boolean verifContenue(int[][] valeur) {
		for (int i = 0; i < valeur.length; i++) {
			for (int j = 0; j < valeur.length; j++) {
				if (valeur[i][j] != 0) {
					return true;
				}
			}
		}
		return false;
	}
	
	
}
