package fr.afpa.Entites;

public class PotionVie extends Objet {

	public PotionVie() {
		super();
	}

	public PotionVie(int valeur, Coordonnee coordonnee) {
		super("potion de vie", valeur, coordonnee, "points de vie");
	}

	
	@Override
	public String toString() {
		return "PotionVie [nom=" + nom + ", valeur=" + valeur + ", coordonnee=" + coordonnee + ", stat=" + stat
				+ ", description=" + description + "]";
	}

}
