package fr.afpa.Entites;

public abstract class Portes {
	
	private String nom;
	private boolean passage;
	private Coordonnee direction;
	
	// Constructeur par defaut
	public Portes() {
		super();
		// TODO Auto-generated constructor stub
	}
	// Constructeur a 3 paramettres
	public Portes(String nom, boolean passage, Coordonnee direction) {
		super();
		this.nom = nom;
		this.passage = passage;
		this.direction = direction;
	}
	// Getter Setter
	public String getNom() {
		return nom;
	}
	public boolean isPassage() {
		return passage;
	}
	public Coordonnee getDirection() {
		return direction;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public void setPassage(boolean passage) {
		this.passage = passage;
	}
	public void setDirection(Coordonnee direction) {
		this.direction = direction;
	}
	// HasCode
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + (passage ? 1231 : 1237);
		return result;
	}
	// equals
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Portes other = (Portes) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (passage != other.passage)
			return false;
		return true;
	}
	// toString
	@Override
	public String toString() {
		return "Portes [nom=" + nom + ", passage=" + passage + "]";
	}
	
}
