DONJON

Auteurs : Astelia, Guillaume et Ali.

Outils utilis�s : Java/ Eclipse/ GitKraken / Bitbucket

Cr�ation d'un jeu de labyrinthe en JAVA.
Le joueur doit rejoindre la sortie du labyrinthe en survivant � l'attaque des monstres plac�s � diff�rents endroits et en r�cup�rant un maximum de pi�ces d'or. 
Il aura la possibilit� d'utiliser des objets pr�sents dans le labyrinthe qui augmenteront ses points de vie ou ses points de force.

Installation : 

1) Cr�er une variable d'environnement qui aura pour nom : DONJON, sa valeur sera le nom du fichier o� seront sauvegard�s les scores.


2) Sur le r�pertoire C cr�er l�arborescence suivante

        ENV
   	|-----donjon_am_gg_ao
	|-------donjon_am_gg_ao.jar

3) Ouvrir l�invite de commande (cmd dans la barre de recherche).

4) D�marrage de l�application, taper les commandes : 
cd C:\ENV\donjon_am_gg_ao
java -jar donjon_am_gg_ao.jar


Fonctionnement de l�application : 

Au lancement de l'application, il vous sera demand� d'entrer les dimensions (2*2 jusqu'� 10*10) du labyrinthe que vous souhaitez r�soudre. 
Entrer le chiffre puis valider avec la touche Entr�e.

3 choix vous seront ensuite propos�s :


1 : Afficher les scores : Pour visualiser les 10 meilleurs scores obtenus par les joueurs (Score en fonction du nombre de pi�ces d'or recup�r�es). 
2 : Demarrer le jeu : Pour lancer une nouvelle partie.
3 : Quitter le jeu 

Choisir le num�ro correspondant � votre choix puis valider.

Entrer votre nom de personnage puis valider.

Un nombre de points de force et de vie vous sera attribu�. 

Vous aurez ensuite le choix de :

1 : Afficher la carte : Pour avoir un aper�u du labyrinthe � traverser avec la position des objets, des monstres ainsi que la sortie du donjon.
2 : Regarder dans une direction : Seule les directions possibles vous seront proposer
3 : Statistiques de 4 : Vos statistiques (points de force, de vie et nombre de pi�ces d'or)
4 : Attaquer le ou les monstre(s) present(s) ou Utiliser les objets de la salle : Avant de vous d�placer ou d'utiliser un objet, il faudra d'abord tuer tous les monstres pr�sents dans la m�me salle que vous.
5 : Se deplacer
0 : Quitter le jeu

A la fin de la partie, votre score sera enregistr� seulement si vous reussissez � atteindre la sortie du labyrinthe. 


Fonctionnalit�es d�velopp�es

G�n�ration al�atoire du donjon.
G�n�ration al�atoire des monstres
G�n�ration al�atoire des objets
G�n�ration du joueur
Affichage des menus � chaque tour
Affichage de la carte du donjon
D�placement des monstres
D�placement du joueur
Utilisation des objets
S�quences de combat
Enregistrement des scores dans la variable d'environnement







