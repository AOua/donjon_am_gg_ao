package fr.afpa.Entites;

public class Monstre extends Personnage{
	
	private String description;
	protected static int autoIncrementationNom = 1;
	
	public Monstre() {
		super();
	}

	public Monstre(int pointDeVie, int force, int pieceDOr, Coordonnee coordonnee, String description) {
		super("Monstre"+autoIncrementationNom++, pointDeVie, force, pieceDOr, coordonnee);
		this.description = description;
		
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Monstre [description=" + description + ", nom=" + nom + ", pointDeVie=" + pointDeVie + ", force="
				+ force + ", pieceDOr=" + pieceDOr + ", coordonnee=" + coordonnee + "]";
	}



	
	

}
