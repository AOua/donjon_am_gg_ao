package fr.afpa.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fr.afpa.Entites.Coordonnee;
import fr.afpa.Entites.Donjon;
import fr.afpa.controle.ControleDonjon;
import fr.afpa.controle.ControleSalle;
import fr.afpa.tests.TestAffichage;

public class DonjonService {

	/**
	 * Fonction qui configure le donjon
	 * 
	 * @param donjon le donjon de base
	 * @return le donjon configuré
	 */
	public static Donjon configurationDonjon(Donjon donjon) {

		// Le nombre de salles dans le donjon
		int nombreSalle = donjon.getDimension() * donjon.getDimension();
		

		// le nom de direction de la salle selectionner
		String[][] direction = new String[donjon.getDimension()][donjon.getDimension()];

		// Un tableau de valeur de cellule pour la creation du donjon
		int[][] valeurCellule = new int[donjon.getDimension()][donjon.getDimension()];
		int ind = 0;
//

		// Mettre des valeurs dans le tableau d'indice pour la creation du lab
		for (int i = 0; i < donjon.getDimension(); i++) {
			for (int j = 0; j < donjon.getDimension(); j++) {
				valeurCellule[j][i] = ind;
				ind++;
			}
		}
		
		// Le tableau d'occurence pour la configuration du donjon
		int numSalleCibler;
		Integer numeroSalleCible = null;

		List<Integer> sallesPasEncoreVisitees = new ArrayList<>();
		for (int i = 0; i < nombreSalle; i++) {
			sallesPasEncoreVisitees.add(i);
		}

		// les Absisse et Ordonnée de la salle cible
		int xSalleCible = 0; // indice x de la salleCibler
		int ySalleCible = 0; // indice y de la salleCibler

		do {
//			
			do {

				// on boucle sur la generation du num de la salle pour eviter l'occurence
				numSalleCibler = new Random().nextInt(sallesPasEncoreVisitees.size());
				numeroSalleCible = sallesPasEncoreVisitees.get(numSalleCibler);
				sallesPasEncoreVisitees.remove(numeroSalleCible);

				for (int x = 0; x < donjon.getDimension(); x++) {
					for (int y = 0; y < donjon.getDimension(); y++) {
						if (numeroSalleCible == donjon.getPlateau()[x][y].getNumero()) {
							xSalleCible = x;
							ySalleCible = y;
						}
					}
				}

			} while (ControleDonjon.verifMax2PortesOuverte(donjon, xSalleCible, ySalleCible));

			int porteAOuvrire = 0;
			boolean laPorteEstSurLExterieurDuPlateau = true;
			

			// la liste de porte vide
			List<Integer> portes = new ArrayList<Integer>();
			// on remplie la liste de porte de 0 a 3
			for (int i = 0; i < 4; i++) {
				portes.add(i);
			}
			int numSalleDelAutreCote = 0;
			Coordonnee coordonnerDeLaSalleDeLAutreCoteDeLaPorte = new Coordonnee();
			boolean verifValeurSalle = false;
			generationPorte : do {
				do {
					int nbAleatoire = 0;

					// on cree un index aleatoirement pour choisir une valeur de porte dans le
					// tableau de porte
					if (portes.size()==0) {
						break generationPorte;
					}
					
					nbAleatoire = new Random().nextInt(portes.size());
					
					// on recupere la valeur de notre porte
					porteAOuvrire = portes.get(nbAleatoire);

					// on retire la porte selectionner du tableau
					portes.remove(nbAleatoire);

					// on verifie si la porte donne sur l'exterieur du plateau
					laPorteEstSurLExterieurDuPlateau = ControleDonjon.controlPorteInterneDonjon(donjon, porteAOuvrire,
							xSalleCible, ySalleCible);
					
				} while (laPorteEstSurLExterieurDuPlateau);
				// le numero de la salle de l'autre cote de la porte
				numSalleDelAutreCote = numSalleDeLAutreCote(porteAOuvrire, donjon, xSalleCible, ySalleCible);

				// les coordonnees de la salle de l'autre cote de la porte
				coordonnerDeLaSalleDeLAutreCoteDeLaPorte = coordonneesDeLaSalleDeLautreCote(xSalleCible, ySalleCible,
						porteAOuvrire);

				// on verifie si la valeur de la salle d'a cote est identique a celle de la
				// salle cible
				verifValeurSalle = ControleDonjon.verifValeurSalle(valeurCellule, xSalleCible, ySalleCible,
						coordonnerDeLaSalleDeLAutreCoteDeLaPorte);
				
			} while (laPorteEstSurLExterieurDuPlateau || verifValeurSalle  );
			
			boolean laPorteEstFermee = false;
			// Controle si la porte selectionne n'est pas ouverte
			laPorteEstFermee = ControleSalle.controlPorteFerme(donjon, porteAOuvrire, xSalleCible, ySalleCible);

			if (laPorteEstFermee && !laPorteEstSurLExterieurDuPlateau) {

				// On ouvre la porte
				donjon.getPlateau()[xSalleCible][ySalleCible].getPorte()[porteAOuvrire].setPassage(true);
				// On ouvre le passage
				ouvrireLePassage(porteAOuvrire, donjon, xSalleCible, ySalleCible);

				// La salle de l'autre coté de la porte
				//int numSalleDeLAutreCote = numSalleDeLAutreCote(porteAOuvrire, donjon, xSalleCible, ySalleCible);

				// on change les valeur du tableau d'indice
				// si l'indice de la salle cible est plus grand que celui de la salle de l'autre
				// cote
				// on change toute les valeurs identique a la salle cible par celle de la salle
				// cible de l'autre cote

				int indiceSalleCible = valeurCellule[xSalleCible][ySalleCible];
				int indiceSalleACote = valeurCellule[coordonnerDeLaSalleDeLAutreCoteDeLaPorte
						.getX()][coordonnerDeLaSalleDeLAutreCoteDeLaPorte.getY()];

				int minValSalle = (indiceSalleCible > indiceSalleACote ? indiceSalleACote : indiceSalleCible);
				int maxValSalle = (indiceSalleCible > indiceSalleACote ? indiceSalleCible : indiceSalleACote);

				for (int i = 0; i < donjon.getDimension(); i++) {
					for (int j = 0; j < donjon.getDimension(); j++) {
						if (valeurCellule[i][j] == maxValSalle) {
							valeurCellule[i][j] = minValSalle;
						}
					}
				}

//				TestAffichage.afficherNumSallesDonjon(valeurCellule);
//				direction[xSalleCible][ySalleCible] = donjon.getPlateau()[xSalleCible][ySalleCible]
//						.getPorte()[porteAOuvrire].getNom();

			}
			if (sallesPasEncoreVisitees.isEmpty() && ControleDonjon.verifContenue(valeurCellule)) {
				sallesPasEncoreVisitees.add(salleARevisiter(valeurCellule));
			} 
			
		} while (ControleDonjon.verifContenue(valeurCellule));

		return donjon;
	}

	/**
	 * Fonction qui ourvre la porte de l'autre cote de la porte ouverte
	 * 
	 * @param porteOuvrerte la porte qui est ouverte
	 * @param donjon        le donjon de base
	 * @param xSalleCible   l'abcisse de la salle cible
	 * @param ySalleCible   l'ordonnée de la salle cible
	 * @return
	 */
	public static boolean ouvrireLePassage(int porteOuvrerte, Donjon donjon, int xSalleCible, int ySalleCible) {

		if (porteOuvrerte == 0 && xSalleCible != 0) {
			donjon.getPlateau()[xSalleCible - 1][ySalleCible].getPorte()[2].setPassage(true);
			return true;
		}
		if (porteOuvrerte == 1 && ySalleCible != donjon.getDimension() - 1) {
			donjon.getPlateau()[xSalleCible][ySalleCible + 1].getPorte()[3].setPassage(true);
			return true;
		}
		if (porteOuvrerte == 2 && xSalleCible != donjon.getDimension() - 1) {
			donjon.getPlateau()[xSalleCible + 1][ySalleCible].getPorte()[0].setPassage(true);
			return true;
		}
		if (porteOuvrerte == 3 && ySalleCible != 0) {
			donjon.getPlateau()[xSalleCible][ySalleCible - 1].getPorte()[1].setPassage(true);
			return true;
		}
		return false;
	}

	/**
	 * Fonction qui retourne le numero de la salle vers lequel le passage est crée
	 * 
	 * @param numPorte    le numero de la porte ouverte
	 * @param donjon      le donjon initial
	 * @param xSalleCible l'abcisse de la salle ciblée
	 * @param ySalleCible l'ordonnée de la salle ciblée
	 * @return
	 */
	public static int numSalleDeLAutreCote(int numPorte, Donjon donjon, int xSalleCible, int ySalleCible) {

		int numSalleDeLAutreCote = 0;
		if (numPorte == 0) {
			numSalleDeLAutreCote = donjon.getPlateau()[xSalleCible - 1][ySalleCible].getNumero();
		}
		if (numPorte == 1) {
			numSalleDeLAutreCote = donjon.getPlateau()[xSalleCible][ySalleCible + 1].getNumero();
		}
		if (numPorte == 2) {
			numSalleDeLAutreCote = donjon.getPlateau()[xSalleCible + 1][ySalleCible].getNumero();
		}
		if (numPorte == 3) {
			numSalleDeLAutreCote = donjon.getPlateau()[xSalleCible][ySalleCible - 1].getNumero();
		}
		return numSalleDeLAutreCote;
	}

	/**
	 * fonction qui retourn les coordonnees de la salle de l'autre cote de la porte
	 * a ouvrire
	 * 
	 * @param xSalleDeLaSalleCible la coordonnee x de la salle cible
	 * @param ySalleDeLaSalleCible la coordonnee y de la salle cible
	 * @param porteAOuvrir         numero de la porte a ouvrire
	 * @return un objet coordonnee qui nous donne kes coordonnee de la salle de
	 *         l'autre cote de la porte
	 */
	public static Coordonnee coordonneesDeLaSalleDeLautreCote(int xSalleDeLaSalleCible, int ySalleDeLaSalleCible,
			int porteAOuvrir) {

		if (porteAOuvrir == 0) {
			xSalleDeLaSalleCible--;
		}
		if (porteAOuvrir == 1) {
			ySalleDeLaSalleCible++;
		}
		if (porteAOuvrir == 2) {
			xSalleDeLaSalleCible++;
		}
		if (porteAOuvrir == 3) {
			ySalleDeLaSalleCible--;
		}
		return new Coordonnee(xSalleDeLaSalleCible, ySalleDeLaSalleCible);
	}
	/**
	 * fonction qui revoie la valeur de la salle a revisiter
	 * @param valeur	tableau de valeur
	 * @return	retourne la valeur de la salle a revisiter
	 */
	public static int salleARevisiter(int[][]valeur) {
		int aRevoir = 0;
		for (int i = 0; i < valeur.length; i++) {
			for (int j = 0; j < valeur.length; j++) {
				if (valeur[i][j]!=0) {
					aRevoir= valeur[i][j];
				} 
			}
		}
	return aRevoir;
	}
}
