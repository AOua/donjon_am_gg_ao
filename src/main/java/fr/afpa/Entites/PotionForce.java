package fr.afpa.Entites;

public class PotionForce extends Objet {

	
	public PotionForce() {
		super();
	}

	public PotionForce(int valeur, Coordonnee coordonnee) {
		super("potion de force", valeur, coordonnee, "points de force");
	}

	@Override
	public String toString() {
		return "PotionForce [nom=" + nom + ", valeur=" + valeur + ", coordonnee=" + coordonnee + ", stat=" + stat
				+ ", description=" + description + "]";
	}

}
