package fr.afpa.Entites;

public class Joueur extends Personnage {

	
	public Joueur() {
		super();
	}

	public Joueur(String nom, int pointDeVie, int force, int pieceDOr, Coordonnee coordonnee) {
		super(nom, pointDeVie, force, pieceDOr, coordonnee);
	}

	@Override
	public String toString() {
		return "Joueur [nom=" + nom + ", pointDeVie=" + pointDeVie + ", force=" + force + ", pieceDOr=" + pieceDOr
				+ ", coordonnee=" + coordonnee + "]";
	}

	


	
	
	

}
