package fr.afpa.services;

import fr.afpa.Entites.BanditManchot;
import fr.afpa.Entites.Objet;
import fr.afpa.Entites.Personnage;
import fr.afpa.controle.ControlePersonnage;
import fr.afpa.ihm.IHMObjet;

public class BanditManchotService {

	/**
	 * Permet d'utiliser le bandit manchot
	 * @param personnage : personnage qui utilise le bandit manchot
	 * @param bm : le bandit manchot utilise 
	 * @param donjon : le donjon ou se trouve le personnage et le bandit manchot
	 */
	public static void utilisationBanditManchot(Personnage personnage, BanditManchot bm) {
		IHMObjet ihmO = new IHMObjet();
		Objet objet = GenerationAleatoireService.aleatoireObjet(personnage.getCoordonnee(), false);
		if (ControlePersonnage.assezDePieceDOr(personnage, bm.getValeur())) {
			personnage.setPieceDOr(personnage.getPieceDOr() - bm.getValeur());
			ihmO.affichageBanditManchotUtiliseOuNon(bm, true, objet);
			ihmO.affichageObjetUtiliseOuNon(objet, ObjetService.effetsDeLObjet(personnage, objet));
		}
		else {
			
			ihmO.affichageBanditManchotUtiliseOuNon(bm, false, objet);
		}
	}

	
	
}
