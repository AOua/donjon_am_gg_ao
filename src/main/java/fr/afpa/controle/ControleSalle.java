package fr.afpa.controle;

import fr.afpa.Entites.Donjon;

public class ControleSalle {
	/**
	 * fonction qui verifie si la porte selectionne est bien ferme
	 * @param donjon 	le donjon initial
	 * @param numPorte	le numero de la porte selectionnee
	 * @param xSalle	l'abscisse de la salle dans le plateau
	 * @param ySalle    l'ordonnée de la salle dans le plateau
	 * @return		un boolean pour verifier si la porte est fermee
	 */
	public static boolean controlPorteFerme (Donjon donjon , int numPorte , int xSalle , int ySalle) {
		if (donjon.getPlateau()[xSalle][ySalle].getPorte()[numPorte].isPassage() == false) {
			return true;
		}	
		return false;
	}
}
