package fr.afpa.services;

import java.util.ArrayList;
import java.util.Random;

import fr.afpa.Entites.BanditManchot;
import fr.afpa.Entites.BourseDOr;
import fr.afpa.Entites.Coordonnee;
import fr.afpa.Entites.Donjon;
import fr.afpa.Entites.Objet;
import fr.afpa.Entites.Personnage;
import fr.afpa.Entites.PotionForce;
import fr.afpa.Entites.PotionVie;
import fr.afpa.ihm.IHMObjet;
import fr.afpa.ihm.SaisieGenerale;

public abstract class ObjetService {

	/**
	 * Permet d'utiliser n'importe quel objet
	 * @param coord : coordonnees du joueur
	 * @param donjon : donjon dans lequel le joueur evolue
	 */
	public static void utilisationDUnObjet(Coordonnee coord, Donjon donjon) { 
		IHMObjet ihmO = new IHMObjet();
		ArrayList<Objet> listeObjetsPresents = listeObjetsDansSalle(coord, donjon.getObjet());
		while (!listeObjetsPresents.isEmpty() && ihmO.demanderUtilisationObjet(donjon.getScanner())) {
			ihmO.affichageObjetsPresents(listeObjetsPresents);
			int choix = SaisieGenerale.saisieEntier(donjon.getScanner());
			if (0<choix && choix<=listeObjetsPresents.size()) {
				Objet objet = listeObjetsPresents.get(choix-1);
				if (objet instanceof BanditManchot) {
					BanditManchotService.utilisationBanditManchot(JoueurService.trouverJoueur(donjon.getPersonnage()), (BanditManchot) objet);
					donjon.getObjet().remove(objet);
					listeObjetsPresents.remove(objet);
				}
				else {
					boolean actionEffectuee = ObjetService.effetsDeLObjet(JoueurService.trouverJoueur(donjon.getPersonnage()), objet);
					if (actionEffectuee) {
						donjon.getObjet().remove(objet);
						listeObjetsPresents.remove(objet);
					}
					ihmO.affichageObjetUtiliseOuNon(objet, actionEffectuee);
				}
			}
			else {
				ihmO.affichageChoixPasDansListe(choix);
			}
		}
		ihmO.affichagePasDobjetsDansSalle(!listeObjetsPresents.isEmpty());
	}
	
	/**
	 * Creation d'une liste d'objets aleatoire en fonction des dimensions
	 * du donjon.
	 * @param dimension : la longueur/largeur du donjon
	 * @return une liste d'objets aleatoire
	 */
	public static ArrayList<Objet> creationListeDObjets(int dimension) {
		ArrayList<Objet> listeObjets = new ArrayList<Objet>();
		for (int i=1; i<=dimension*5; i++) {
			int coordX = new Random().nextInt(dimension);
			int coordY = new Random().nextInt(dimension);
			listeObjets.add(GenerationAleatoireService.aleatoireObjet(new Coordonnee(coordX, coordY), true));
		}
		return listeObjets;
	}
	
	/**
	 * Permet de retourner la liste des objets presente aux coordonnee passees en parametre
	 * @param coord : les coordonnes de la salle dont on cherche a connaitre les objets contenus
	 * @param listeObjetsDonjon : la liste des objets du donjon
	 * @return la liste des objets contenu dans la salle aux coordonnees indiquees
	 */
	public static ArrayList<Objet> listeObjetsDansSalle(Coordonnee coord, ArrayList<Objet> listeObjetsDonjon) {
		ArrayList<Objet> listeObjetsSalle = new ArrayList<Objet>();
		for (Objet objet : listeObjetsDonjon) {
			if (objet.getCoordonnee().equals(coord)) {
				listeObjetsSalle.add(objet);
			}
		}
		return listeObjetsSalle;
	}
	
	/**
	 * Permet de compter le nombre d'objets dans une salle
	 * @param coord : les coordonnes de la salle dont on cherche a compter le nombre d'objets presents
	 * @param listeObjetsDonjon : la liste des objets du donjon
	 * @return le nombre d'objets presents dans la salle
	 */
	public static int compterNombreObjetsDansSalle(Coordonnee coord, ArrayList<Objet> listeObjetsDonjon) {
		return listeObjetsDansSalle(coord, listeObjetsDonjon).size();
	}
	
	/**
	 * Permet d'utiliser un objet hors bandit manchot
	 * @param personnage : le personnage qui utilise l'objet
	 * @param objet : l'objet utilise
	 * @return true si l'objet a ete utilise et false sinon
	 */
	public static boolean effetsDeLObjet(Personnage personnage, Objet objet) {
		if (objet instanceof BourseDOr) {
			return BourseDOrService.effetsDeLObjet(personnage, objet);
		}
		else if (objet instanceof PotionVie) {
			return PotionVieService.effetsDeLObjet(personnage, objet);
		}
		else if (objet instanceof PotionForce) {
			return PotionForceService.effetsDeLObjet(personnage, objet);
		}
		return false;
	}
	
}
