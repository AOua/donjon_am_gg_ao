package fr.afpa.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;

import fr.afpa.Entites.Score;

public class LectureEcritureFichierDeSauvegarde {
	
	/**
	 * Permet de retourner la liste de scores contenu dans un fichier
	 * @return la liste de scores contenus dans un fichier
	 */
	public static ArrayList<Score> lireFichier() {
		ArrayList<String> texte = lectureFichier(System.getenv("DONJON"));
		ArrayList<Score> listeScores = new ArrayList<Score>();
		for (String ligne : texte) {
			String[] res = ligne.split(";");
			Score score = new Score(res[0], Integer.parseInt(res[1]), LocalDateTime.parse(res[2]));
			listeScores.add(score);
		}
		return listeScores;
	}
	
	/**
	 * Permet de lire un fichier
	 * @param fileName : le nom du fichier
	 * @return le contenu du fichier
	 */
	private static ArrayList<String> lectureFichier(String fileName) {
		ArrayList<String> texte = new ArrayList<String>();
		BufferedReader br = null;
		try {
			FileReader fr = new FileReader(fileName);
			br = new BufferedReader(fr);
			while (br.ready()) {
				texte.add(br.readLine());
			}
			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("Fichier introuvable : Impossible de lire les 10 meilleurs scores");
		} catch (IOException e) {
			System.out.println("Erreur d'entree/sortie");
		} catch (NullPointerException npe) {
			System.out.println("La variable d'environnement 'DONJON' n'a pas ete creee ! ");
			System.out.println("Vous ne pouvez pas lire de score.");
		}
		return texte;
	}
	
	/**
	 * Permet d'ecrire dans un fichier la liste des 10 premiers scores
	 * @param listeScores : la liste des scores a ecrire
	 * @return true si la liste a ete ecrite et false sinon
	 */
	public static boolean ecrireFichier(ArrayList<Score> listeScores) {
		Collections.sort(listeScores);
		ArrayList<String> texte = new ArrayList<String>();
		for (int i=0; i<listeScores.size() && i<10; i++) {
			String ligne = listeScores.get(i).getNom()+";"+listeScores.get(i).getScore()+";"+listeScores.get(i).getDate();
			texte.add(ligne);
		}
		return ecritureFichier(texte, System.getenv("DONJON"));
	}
	
	/**
	 * Permet d'ecrire du texte dans un fichier
	 * @param texte : le texte a ecrire 
	 * @param fileName : le nom du fichier dans lequel on ecrit le texte
	 * @return true si le texte a ete ecrit dans le fichier et false sinon
	 */
	private static boolean ecritureFichier(ArrayList<String> texte, String fileName) {
		try {
			FileWriter fw = new FileWriter(fileName, false);
			BufferedWriter bw = new BufferedWriter(fw);
			for (String string : texte) {
				bw.write(string);
				bw.newLine();
			}
			bw.close();
			return true;
		} catch (IOException e) {
			System.out.println("Fichier introuvable : Impossible d'enregistrer un score");
			return false;
		} catch (NullPointerException npe) {
			System.out.println("La variable d'environnement 'DONJON' n'a pas ete creee ! ");
			System.out.println("Le score n'a pas pu etre enregistre.");
			return false;
		}
		
	}
	
}
