package fr.afpa.ihm;

import java.util.ArrayList;
import java.util.Scanner;

import fr.afpa.Entites.BanditManchot;
import fr.afpa.Entites.Objet;

public class IHMObjet {

	/**
	 * Permet de demander a l'utilisateur s'il ne veut pas utiliser d'objet
	 * @param scanner : source de la saisie
	 * @return true si l'utilisateur entre oui et false s'il entre non
	 */
	public boolean demanderUtilisationObjet(Scanner scanner) {
		System.out.print("Voulez-vous utiliser un autre objet ? (oui/non) : ");
		return SaisieGenerale.saisieBoolean(scanner);
	}
	
	/**
	 * Permet d'afficher si un objet a ete utilise ou non
	 * @param objet : l'objet a utiliser
	 * @param objetUtilise : indique si l'objet a ete utilise ou non
	 */
	public void affichageObjetUtiliseOuNon(Objet objet, boolean objetUtilise) {
		if (objetUtilise) {
			System.out.println("La "+objet.getNom()+" a ete utilisee. "+objet.getValeur()+" "+objet.getStat()+" ont ete ajoutes.");
		}
		else {
			System.out.println("La "+objet.getNom()+"a ete laissee sur place.");
		}
	}
	
	/**
	 * Permet d'afficher si le bandit manchot a ete utiise ou non
	 * @param bm : le bandit manchot gere
	 * @param bmUtilise : true si le bandit manchot a ete utilise et false sinon
	 */
	public void affichageBanditManchotUtiliseOuNon(BanditManchot bm, boolean bmUtilise, Objet objet) {
		if (bmUtilise) {
			System.out.println("Vous mettez de l'argent dans le "+bm.getNom()+".");
			System.out.println("Le "+bm.getNom()+" delivre une "+objet.getNom()+".");
		}
		else {
			System.out.println("Vous n'avez pas assez d'argent pour utiliser le "+bm.getNom());
		}
	}
	
	/**
	 * Permet d'afficher que des objets n'ont pas ete utilises ou qu'il
	 * n'y avait pas d'objets dans la salle
	 * @param objetsPresents : true s'il y a des obets dans la salle et false sinon
	 */
	public void affichagePasDobjetsDansSalle(boolean objetsPresents) {
		if (objetsPresents) {
			System.out.println("Le reste des objets a ete laisse sur place.");
		}
		else {
			System.out.println("Il n'y a plus d'objets dans la salle !");
		}
	}
	
	/**
	 * Permet d'afficher le menu d'utilisation des objets
	 * @param listeObjets
	 */
	public void affichageObjetsPresents(ArrayList<Objet> listeObjets) {
		System.out.println("Quel objet souhaitez-vous utiliser ? :");
		for (int i=0; i<listeObjets.size(); i++) {
			Objet objet = listeObjets.get(i);
			String nom = objet.getNom().toUpperCase().charAt(0)+objet.getNom().substring(1);
			System.out.println((i+1)+" - "+nom+" : "+objet.getValeur()+" "+objet.getStat());
		}
		System.out.print("> ");
	}
	
	/**
	 * Permet d'afficher qu'un choix n'est pas dans la liste
	 * @param choix : le choix effectue par le joueur
	 */
	public void affichageChoixPasDansListe(int choix) {
		System.out.println("Le choix numero "+choix+" n'est pas dans la liste !");
	}
	
}
