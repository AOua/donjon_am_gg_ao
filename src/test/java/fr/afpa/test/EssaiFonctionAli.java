package fr.afpa.test;

import java.util.Scanner;

import fr.afpa.Entites.Coordonnee;
import fr.afpa.Entites.Donjon;
import fr.afpa.ihm.IHMAffichageDonjon;
import fr.afpa.services.JoueurService;
import fr.afpa.services.ObjetService;
import fr.afpa.services.PersonnageService;

public class EssaiFonctionAli {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int dimension = 5;

		Donjon donjon = new Donjon(dimension, scanner);

		for (int i = 0; i < donjon.getPlateau().length - 1; i++) {
			for (int j = 0; j < donjon.getPlateau()[0].length - 1; j++) {
				donjon.getPlateau()[3][j].getPorte()[2].setPassage(true);
			}
			donjon.getPlateau()[i][0].getPorte()[1].setPassage(true);
		}

		donjon.setObjet(ObjetService.creationListeDObjets(dimension));
		donjon.setPersonnage(PersonnageService.creationListeDePersonnage(dimension));

		//JoueurService.descriptionSalle(new Coordonnee(0, 0), donjon);
		IHMAffichageDonjon.affichageCarteDonjon(donjon);

		scanner.close();
	}

}
