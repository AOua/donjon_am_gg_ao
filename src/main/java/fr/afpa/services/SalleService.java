package fr.afpa.services;

import java.util.ArrayList;

import fr.afpa.Entites.Coordonnee;
import fr.afpa.Entites.Donjon;
import fr.afpa.Entites.Portes;

public class SalleService {

	/**
	 * Permet de retourner une liste des portes disponibles aux coordonnees entrees en parametre
	 * @param coord : les coordonnees de la salle dont on veut connaitre les directions possibles
	 * @param donjon : le donjon ou evolue le joueur
	 * @return : la liste des directions disponibles d'une salle
	 */
	public static ArrayList<Portes> directionsDiponibles(Coordonnee coord, Donjon donjon) {
		ArrayList<Portes> listePortesDispo = new ArrayList<Portes>();
		for (Portes porte : donjon.getPlateau()[coord.getX()][coord.getY()].getPorte()) {
			if (porte.isPassage()) {
				listePortesDispo.add(porte);
			}
		}
		return listePortesDispo;
	}
	
}
