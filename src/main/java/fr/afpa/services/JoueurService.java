package fr.afpa.services;

import java.util.ArrayList;

import fr.afpa.Entites.Coordonnee;
import fr.afpa.Entites.Donjon;
import fr.afpa.Entites.Joueur;
import fr.afpa.Entites.Monstre;
import fr.afpa.Entites.Personnage;

import fr.afpa.controle.ControlePersonnage;
import fr.afpa.ihm.IHMPersonnage;

public class JoueurService extends PersonnageService {

	/**
	 * Méthode qui va vérifier que le joueur va pouvoir attaquer s'il rencontre un
	 * monstre
	 * 
	 * @param personnage   Joueur
	 * @param listeMonstre
	 * @return Vrai si le joueur se trouve dans la meme salle qu'un monstre
	 */

	public static boolean attaquer(Personnage personnage, Personnage monstre) {

		if (personnage instanceof Joueur) {
			PersonnageService.pertePointDeVie(monstre, personnage);
			return true;
		}

		return false;

	}

	public static boolean riposter(Personnage personnage, Personnage monstre) {

		if (monstre instanceof Monstre) {
			PersonnageService.pertePointDeVie(personnage, monstre);
			return true;
		}

		return false;

	}

	/**
	 * Méthode qui va vérifier qui va permettre au joueur de se déplacer si aucun
	 * monstre n'est present dans la salle
	 * 
	 * @param personnage   Joueur
	 * @param listeMonstre
	 * @return Vrai si un déplacement est possible
	 */
	public static boolean actionDeplacer(Personnage personnage, Coordonnee nouvelleDirection) {

			personnage.setCoordonnee(
					CoordonneeService.calculerNouvelleCoordonnee(personnage.getCoordonnee(), nouvelleDirection));
			return true;
		
	}

	
	public static boolean seDeplacer(Donjon donjon) {
		
		return actionDeplacer(JoueurService.trouverJoueur(donjon.getPersonnage()), IHMPersonnage.choixDirectionDeplacement(donjon));

	}
	
	/**
	 * Méthode qui va renvoyer les informations de la salle
	 * 
	 * @param coordonnee de la salle où obtenir les informations
	 * @param donjon
	 */

	public static void actionDescriptionSalle(Coordonnee coordonnee, Donjon donjon) {
		
		Personnage joueur = JoueurService.trouverJoueur(donjon.getPersonnage());
		Coordonnee nouvCoord = CoordonneeService.calculerNouvelleCoordonnee(joueur.getCoordonnee(), coordonnee);
		IHMPersonnage.affichageInfoMonstre(donjon.getPersonnage(), nouvCoord);

		IHMPersonnage.affichageInfoObjet(donjon.getObjet(), nouvCoord);

		IHMPersonnage.affichageDirectionPossible(nouvCoord, donjon);

	}
	
	public static void descriptionSalle(Donjon donjon) {
		
		 actionDescriptionSalle(IHMPersonnage.choixDirectionRegard(donjon), donjon);
		
	}

	/**
	 * Méthode qui va renvoyer la liste des monstres présents dans la salle
	 *
	 * @param coordonnee   Vers laquelle le joueur va regarder
	 * @param listeMonstre
	 * @return la liste des monstres présents dans la salle
	 */
	public static ArrayList<Personnage> regarderMonstre(Coordonnee coordonnee, ArrayList<Personnage> listeMonstre) {

		ArrayList<Personnage> listPersonnage = new ArrayList<Personnage>();

		if (ControlePersonnage.presenceMonstre(coordonnee, listeMonstre)) {
			for (Personnage personnage : listeMonstre) {
				if (personnage instanceof Monstre && personnage.getCoordonnee().equals(coordonnee)) {
					listPersonnage.add(personnage);

				}

			}

		}

		return listPersonnage;

	}

	/**
	 * Méthode qui va retourner le joueur
	 * 
	 * @param listePersonnage
	 * @return le joueur
	 */
	public static Joueur trouverJoueur(ArrayList<Personnage> listePersonnage) {

		for (Personnage personnage : listePersonnage) {

			if (personnage instanceof Joueur) {
				return (Joueur) personnage;
			}
		}
		return null;

	}

	/**
	 * Permet de compter le nombre de monstres dans une salle
	 * 
	 * @param coordonnee:        les coordonnes de la salle
	 * @param listeMonstreDonjon : la liste des monstres du donjon
	 * @return le nombre de monstres presents dans la salle
	 */
	public static int compterNombreMonstresDansSalle(Coordonnee coordonnee, ArrayList<Personnage> listeMonstreDonjon) {
		for (Personnage personnage : listeMonstreDonjon) {
			if (personnage instanceof Monstre) {
				return regarderMonstre(coordonnee, listeMonstreDonjon).size();

			}

		}
		return 0;
	}

}
