package fr.afpa.Entites;

public class MonstreDeplacementVersLeJoueur extends MonstreDeplacementUniquementAleatoire{

	public MonstreDeplacementVersLeJoueur() {
		super();
	}

	public MonstreDeplacementVersLeJoueur(int pointDeVie, int force, int pieceDOr, Coordonnee coordonnee,
			String description) {
		super(pointDeVie, force, pieceDOr, coordonnee, description);
		this.nom = "MonstreVersJoueur"+autoIncrementationNom++;
	}

	@Override
	public String toString() {
		return "MonstreDeplacementVersLeJoueur [nom=" + nom + ", pointDeVie=" + pointDeVie + ", force=" + force
				+ ", pieceDOr=" + pieceDOr + ", coordonnee=" + coordonnee + "]";
	}

	
}
