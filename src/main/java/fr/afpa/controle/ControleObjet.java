package fr.afpa.controle;

import java.util.ArrayList;

import fr.afpa.Entites.Coordonnee;
import fr.afpa.Entites.Objet;

public class ControleObjet {

	/**
	 * Permet de verifier si a la coordonnee passee en parametre, il  y a un objet
	 * @param coordonnee : la position de la salle dans laquelle on souhaite verifier la presence d'objets 
	 * @param listeDObjets : la liste de tous les objets presents dans le donjon
	 * @return true s'il y a un objet dans a la coordonnee donnee et false sinon
	 */
	public static boolean presenceObjet(Coordonnee coordonnee, ArrayList<Objet> listeDObjets) {
		for (Objet objet : listeDObjets) {
			if (objet.getCoordonnee().equals(coordonnee)) {
				return true;
			}
		}
		return false;
	}
	
}
