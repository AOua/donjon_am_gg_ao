package fr.afpa.services;

import java.time.LocalDateTime;
import java.util.ArrayList;

import fr.afpa.Entites.Donjon;
import fr.afpa.Entites.Joueur;
import fr.afpa.Entites.Personnage;
import fr.afpa.Entites.Score;
import fr.afpa.controle.ControleMenu;
import fr.afpa.controle.ControlePersonnage;
import fr.afpa.ihm.IHMAffichageDonjon;
import fr.afpa.ihm.IHMMenu;
import fr.afpa.ihm.IHMPersonnage;
import fr.afpa.ihm.SaisieGenerale;

public class MenuService {

	/**
	 * Permet d'executer le menu de demarrage du jeu
	 * @param donjon : donjon dans lequel le joueur va evoluer
	 */
	public static void demarrage(Donjon donjon) {
		IHMMenu.affichageDemarrageJeu(donjon);
		menu(donjon);
	}
	
	/**
	 * Permet d'executer le menu principal
	 * @param donjon : le donjon ou evolue le joueur
	 */
	public static void menu(Donjon donjon) {
		Joueur joueur = JoueurService.trouverJoueur(donjon.getPersonnage());
		while (!(ControleMenu.gagne(joueur, donjon) || ControleMenu.perdu(joueur))) {
			IHMMenu.menuPrincipal(donjon);
			String choix = SaisieGenerale.saisieString(donjon.getScanner());
			if (ControlePersonnage.presenceMonstre(joueur.getCoordonnee(), donjon.getPersonnage())) {
				menuMonstresPresents(choix, donjon);
			}
			else {
				menuSansMonstres(choix, donjon);
			}
			joueur = JoueurService.trouverJoueur(donjon.getPersonnage());
		}
		if (ControleMenu.perdu(joueur)) {
			IHMMenu.perdu();
		}
		else {
			IHMMenu.gagne();
			enregistrementScore(joueur);
		}
	}
	
	/**
	 * Permet d'executer le menu dedie lorsqu'il n'y ap pas de monstre
	 * @param choix : le choix d'action de l'utilisateur
	 * @param donjon : le donjon ou evolue le joueur
	 */
	public static void menuSansMonstres(String choix, Donjon donjon) {
		switch (choix) {
			case "1" : IHMAffichageDonjon.affichageCarteDonjon(donjon);
				break;
			case "2" : JoueurService.descriptionSalle(donjon);
				break;
			case "3" : IHMPersonnage.affichageStats(JoueurService.trouverJoueur(donjon.getPersonnage()), "tout");
				break;
			case "4" : ObjetService.utilisationDUnObjet(JoueurService.trouverJoueur(donjon.getPersonnage()).getCoordonnee(), donjon);
					   MonstreService.monstreDeplacementAleatoire(donjon);
				break;
			case "5" : JoueurService.seDeplacer(donjon);
					   MonstreService.monstreDeplacementAleatoire(donjon);
				break;
			case "0" : quitterLeJeu(donjon);
				break;
		}
	}
	
	/**
	 * Permet d'executer le menu dedie lorsqu'il y a au moins un monstre
	 * @param choix : le choix d'action de l'utilisateur
	 * @param donjon : le donjon ou evolue le joueur
	 */
	public static void menuMonstresPresents(String choix, Donjon donjon) {
		switch (choix) {
			case "1" : IHMAffichageDonjon.affichageCarteDonjon(donjon);
				break;
			case "2" : JoueurService.descriptionSalle(donjon);
				break;
			case "3" : IHMPersonnage.affichageStats(JoueurService.trouverJoueur(donjon.getPersonnage()), "tout");
				break;
			case "4" : PersonnageService.combatPlusieursMonstres(JoueurService.trouverJoueur(donjon.getPersonnage()), donjon.getPersonnage());
					   MonstreService.monstreDeplacementAleatoire(donjon);
				break;
			case "0" : quitterLeJeu(donjon);
				break;
		}
	}
	
	/**
	 * Permet d'executer le menu ou l'on peut voir les scores
	 * @param donjon : le donjon ou evoluera le joueur
	 */
	public static void menuScores(Donjon donjon) {
		IHMMenu.affichageMenuScores();
		String choix = SaisieGenerale.saisieString(donjon.getScanner());
		if ("1".equalsIgnoreCase(choix)) {
			IHMMenu.affichageScores(LectureEcritureFichierDeSauvegarde.lireFichier());
			menuScores(donjon);
		}
		else if ("2".equals(choix)) {
			demarrage(donjon);
		}
		else if (!IHMMenu.demanderQuitterLeJeu(donjon)) {
			menuScores(donjon);
		}
	}
	
	/**
	 * Permet de quitter le jeu si l'utilisateur le desire
	 * @param donjon : le donjon dans lequel le joueur evolue
	 */
	public static void quitterLeJeu(Donjon donjon) {
		if (IHMMenu.demanderQuitterLeJeu(donjon)) {
			JoueurService.trouverJoueur(donjon.getPersonnage()).setPointDeVie(0);
		}
	}
	
	/**
	 * Permet d'enregistrer un score si le joueur a gagne une partie
	 * @param joueur : le joueur
	 * @return true si l'ecriture a ete effectuee et false sinon
	 */
	public static boolean enregistrementScore(Personnage joueur) {
		ArrayList<Score> listeScores = LectureEcritureFichierDeSauvegarde.lireFichier();
		listeScores.add(new Score(joueur.getNom(), joueur.getPieceDOr(), LocalDateTime.now()));
		return LectureEcritureFichierDeSauvegarde.ecrireFichier(listeScores);
	}
	
}
