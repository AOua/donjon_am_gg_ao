package fr.afpa.Entites;

import java.time.LocalDateTime;

public class Score implements Comparable {

	private String nom;
	private int score;
	private LocalDateTime date;
	
	
	public Score() {
		super();
	}

	public Score(String nom, int score, LocalDateTime date) {
		super();
		this.nom = nom;
		this.score = score;
		this.date = date;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + score;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Score))
			return false;
		Score other = (Score) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (score != other.score)
			return false;
		return true;
	}

	/**
	 * Pour comparer par rapport au score, de maniere decroissante
	 */
	@Override
	public int compareTo(Object o) {
		if (this.score < ((Score) o).score) {
			return 1;
		}
		else if (this.score > ((Score) o).score) {
			return -1;
		}
		else {
			return 0;
		}
	}

	@Override
	public String toString() {
		return "Score [nom=" + nom + ", score=" + score + ", date=" + date + "]";
	}
}
