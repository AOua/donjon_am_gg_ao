package fr.afpa.services;

import fr.afpa.Entites.Coordonnee;

public class CoordonneeService {

	/**
	 * Methode qui va calculer les nouvelles coordonnées
	 * @param coordonnee actuelle
	 * @param direction visée
	 * @return les nouvelles coordonnées 
	 */
	public static Coordonnee calculerNouvelleCoordonnee(Coordonnee coordonnee, Coordonnee direction) {
		
		int coordonneeX = coordonnee.getX() + direction.getX();
		int coordonneeY = coordonnee.getY() + direction.getY();
		
		return new Coordonnee(coordonneeX, coordonneeY);
		
	}
}
