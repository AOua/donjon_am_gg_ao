package fr.afpa.ihm;

import java.util.ArrayList;

import fr.afpa.Entites.BanditManchot;
import fr.afpa.Entites.BourseDOr;
import fr.afpa.Entites.Coordonnee;
import fr.afpa.Entites.Donjon;
import fr.afpa.Entites.Joueur;
import fr.afpa.Entites.Monstre;
import fr.afpa.Entites.MonstreDeplacementUniquementAleatoire;
import fr.afpa.Entites.MonstreDeplacementVersLeJoueur;
import fr.afpa.Entites.Objet;
import fr.afpa.Entites.Personnage;
import fr.afpa.Entites.PorteEst;
import fr.afpa.Entites.PorteNord;
import fr.afpa.Entites.PorteSud;
import fr.afpa.Entites.PorteWest;
import fr.afpa.Entites.PotionForce;
import fr.afpa.Entites.PotionVie;
import fr.afpa.controle.ControlePersonnage;

import fr.afpa.services.SalleService;

public class IHMPersonnage {

	/**
	 * Méthode qui affiche les informations des monstres présents dans une salle
	 * 
	 * @param listeMonstre
	 */
	public static void affichageInfoMonstre(ArrayList<Personnage> listeMonstre, Coordonnee coordonnee) {

		for (Personnage personnage : listeMonstre) {
			if (personnage instanceof MonstreDeplacementVersLeJoueur && personnage.getCoordonnee().equals(coordonnee)) {
				System.out.println("Information du Monstre");
				System.out.println("Nom : " + personnage.getNom());
				System.out.println("Point de vie : " + personnage.getPointDeVie());
				System.out.println("Force : " + personnage.getForce());
				System.out.println("Nombre de pièces d'or : " + personnage.getPieceDOr());
				System.out.println(((Monstre) personnage).getDescription());
				System.out.println();
			}

			else if (personnage instanceof MonstreDeplacementUniquementAleatoire && personnage.getCoordonnee().equals(coordonnee)) {
				System.out.println("Information du Monstre");
				System.out.println("Nom : " + personnage.getNom());
				System.out.println("Point de vie : " + personnage.getPointDeVie());
				System.out.println("Force : " + personnage.getForce());
				System.out.println("Nombre de pièces d'or : " + personnage.getPieceDOr());
				System.out.println(((Monstre) personnage).getDescription());
				System.out.println();
			}
			else if (personnage instanceof Monstre && personnage.getCoordonnee().equals(coordonnee)) {
				System.out.println("Information du Monstre");
				System.out.println("Nom : " + personnage.getNom());
				System.out.println("Point de vie : " + personnage.getPointDeVie());
				System.out.println("Force : " + personnage.getForce());
				System.out.println("Nombre de pièces d'or : " + personnage.getPieceDOr());
				System.out.println(((Monstre) personnage).getDescription());
				System.out.println();
			}
		}

	}

	/**
	 * Méthode qui affiche la valeur des objets présents dans une salle
	 * 
	 * @param listeObjet
	 */
	public static void affichageInfoObjet(ArrayList<Objet> listeObjet, Coordonnee coordonnee) {

		for (Objet objet : listeObjet) {
			if (objet instanceof PotionVie && objet.getCoordonnee().equals(coordonnee)) {
				System.out.println(
						"Cette potion de vie vous rapporte " + ((PotionVie) objet).getValeur() + " points de vie.");

			} else if (objet instanceof PotionForce && objet.getCoordonnee().equals(coordonnee)) {
				System.out.println("Cette potion de force vous rapporte " + ((PotionForce) objet).getValeur()
						+ " points de force.");

			} else if (objet instanceof BourseDOr && objet.getCoordonnee().equals(coordonnee)) {
				System.out.println("Cette bourse vous rapporte " + ((BourseDOr) objet).getValeur() + " pièces d'or.");

			} else if (objet instanceof BanditManchot && objet.getCoordonnee().equals(coordonnee)) {
				System.out.println("Ce bandit manchot coûte à l'utilisation " + ((BanditManchot) objet).getValeur()
						+ " pièces d'or.");
			}
		}

	}

	/**
	 * Méthode qui affiche le nom des portes ouvertes
	 * 
	 * @param coordonnee de la salle à vérifier
	 * @param donjon
	 */
	public static void affichageDirectionPossible(Coordonnee coordonnee, Donjon donjon) {

		for (int i = 0; i < SalleService.directionsDiponibles(coordonnee, donjon).size(); i++) {

			System.out.println("La porte " + SalleService.directionsDiponibles(coordonnee, donjon).get(i).getNom()
					+ " est ouverte");

		}
	}

	/**
	 * Méthode qui demande au joueur le choix de la direction vers laquelle il
	 * souhaite regarder
	 * 
	 * @param donjon
	 * @return La coordonnée de la direction souhaité
	 */
	public static Coordonnee choixDirectionRegard(Donjon donjon) {
		IHMMenu.menuDirection(donjon, true);
		String choix = SaisieGenerale.saisieString(donjon.getScanner()).toLowerCase();

		if (ControlePersonnage.controleDirectionPossible(choix, donjon) || "ici".equals(choix)) {

			switch (choix) {
			case "nord":
				return new PorteNord().getDirection();

			case "est":
				return new PorteEst().getDirection();

			case "sud":
				return new PorteSud().getDirection();

			case "ouest":
				return new PorteWest().getDirection();

			case "ici":
				return new Coordonnee(0, 0);
			}

		}
		System.out.println("Direction indisponible");
		return choixDirectionRegard(donjon);

	}

	/**
	 * Méthode qui demande au joueur le choix de la direction vers laquelle il
	 * souhaite se déplacer
	 * 
	 * @param donjon
	 * @return La cooronnée de la direction souhaité
	 */
	public static Coordonnee choixDirectionDeplacement(Donjon donjon) {

		IHMMenu.menuDirection(donjon, false);
		String choix = SaisieGenerale.saisieString(donjon.getScanner()).toLowerCase();
		if (ControlePersonnage.controleDirectionPossible(choix, donjon)) {

			switch (choix) {
			case "nord":
				return new PorteNord().getDirection();

			case "est":
				return new PorteEst().getDirection();

			case "sud":
				return new PorteSud().getDirection();

			case "ouest":
				return new PorteWest().getDirection();

			}
		}
		System.out.println("Direction indisponible");
		return new Coordonnee(0, 0);
	}

	/**
	 * Affichage avant un combat
	 * 
	 * @param joueur  Héros
	 * @param monstre
	 */
	public static void affichageDebutCombat(Personnage joueur, Personnage monstre) {

		System.out.println(
				"Le héros a " + joueur.getPointDeVie() + " points de vie et " + joueur.getForce() + " points de force");
		System.out.println("Le héros combat le " + monstre.getNom());
		System.out.println("Le " + monstre.getNom() + " a " + monstre.getPointDeVie() + "points de vie et a "
				+ monstre.getForce() + " points de force.");

	}

	/**
	 * Affichage à la fin du combat
	 * 
	 * @param joueur  Héros
	 * @param monstre
	 */
	public static void affichageFinCombat(Personnage joueur, Personnage monstre) {

		if (joueur instanceof Joueur && ControlePersonnage.enVie(joueur)) {
			System.out.println("Le héros a gagné le combat, il lui reste " + joueur.getPointDeVie() + " points de vie");
			System.out.println("Il récupère : " + monstre.getPieceDOr() + " pièces d'or.");
			affichageStats(joueur, "po");
			System.out.println("");
		} else {

			System.out.println("Le héros n'a plus de point de vie.");
		}
	}
	
	/**
	 * Permet d'afficher les stats d'un personnage (surtout le joueur)
	 * @param joueur : le personnage dont on veut afficher les statistiques
	 * @param stat : "pv" -> affiche les points de vie restant
	 * 				 "pf" -> affiche les points de force 
	 * 				 "po" -> affiche les pieces d'or
	 * 				 par defaut -> affiche toutes les statistiques
	 */
	public static void affichageStats(Personnage joueur, String stat) {
		switch (stat) {
			case "pv" : System.out.println("Il reste "+joueur.getPointDeVie()+" points de vie a "+joueur.getNom()+".");
				break;
			case "pf" : System.out.println("Il reste "+joueur.getForce()+" points de force a "+joueur.getNom()+".");
				break;
			case "po" : System.out.println("Il reste "+joueur.getPieceDOr()+" pieces d'or a "+joueur.getNom()+".");
				break;
			default : System.out.println("Statistiques de "+joueur.getNom()
										+"\n"+"Points de vie : "+joueur.getPointDeVie()
										+"\n"+"Points de force : "+joueur.getForce()
										+"\n"+"Pieces d'or : "+joueur.getPieceDOr());
		}
	}

}
