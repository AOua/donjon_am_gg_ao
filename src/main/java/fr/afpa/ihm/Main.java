package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.Entites.Donjon;
import fr.afpa.Entites.SalleSortie;
import fr.afpa.services.DonjonService;
import fr.afpa.services.MenuService;


public class Main {

	public static void main(String[] args) {
		

		Scanner scanner = new Scanner(System.in);
		int dimension = IHMMenu.demandeDimension(scanner);		
		Donjon donjon = new Donjon(dimension, scanner);
		DonjonService.configurationDonjon(donjon);
		donjon.getPlateau()[dimension-1][dimension-1] = new SalleSortie();
		MenuService.menuScores(donjon);
		scanner.close();	
		
}
	
	public static boolean affichage(Donjon donjon) {
		if (donjon != null) {
			
		for (int i = 0; i < donjon.getDimension(); i++) {
			for (int j = 0; j < donjon.getDimension(); j++) {
				System.out.print(donjon.getPlateau()[i][j].getNumero());
			}
			//System.out.println(Salle.autoIncremente);
			System.out.println();
		}
		return true;
		}
		return false;
	}

}
