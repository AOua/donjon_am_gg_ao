package fr.afpa.Entites;

public abstract class Personnage {
	
	protected String nom;
	protected int pointDeVie; 
	protected int force;
	protected int pieceDOr;
	protected Coordonnee coordonnee;
	

	public Personnage() {
		super();
	}

	public Personnage(String nom, int pointDeVie, int force, int pieceDOr, Coordonnee coordonnee) {
		super();
		this.nom = nom;
		this.pointDeVie = pointDeVie;
		this.force = force;
		this.pieceDOr = pieceDOr;
		this.coordonnee = coordonnee;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPointDeVie() {
		return pointDeVie;
	}

	public void setPointDeVie(int pointDeVie) {
		this.pointDeVie = pointDeVie;
	}

	public int getForce() {
		return force;
	}

	public void setForce(int force) {
		this.force = force;
	}

	public int getPieceDOr() {
		return pieceDOr;
	}

	public void setPieceDOr(int pieceDOr) {
		this.pieceDOr = pieceDOr;
	}

	public Coordonnee getCoordonnee() {
		return coordonnee;
	}

	public void setCoordonnee(Coordonnee coordonnee) {
		this.coordonnee = coordonnee;
	}

	
	@Override
	public abstract String toString();
}
