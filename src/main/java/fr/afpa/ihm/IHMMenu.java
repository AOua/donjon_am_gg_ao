package fr.afpa.ihm;

import java.util.ArrayList;
import java.util.Scanner;

import fr.afpa.Entites.Coordonnee;
import fr.afpa.Entites.Donjon;
import fr.afpa.Entites.Joueur;
import fr.afpa.Entites.Personnage;
import fr.afpa.Entites.Portes;
import fr.afpa.Entites.Score;
import fr.afpa.controle.ControlePersonnage;
import fr.afpa.services.JoueurService;
import fr.afpa.services.SalleService;

public class IHMMenu {

	public static int demandeDimension(Scanner scanner) {
		
		System.out.println("Le donjon Darksaroth qui fait peur de la mort qui tue !!!");
		System.out.println();
		System.out.println("Quelle dimension aura le donjon ? (2 - 10) : ");
		System.out.print("> ");
		int dimension = SaisieGenerale.saisieEntier(scanner);
		if (!(2<=dimension && dimension<=10)) {
			dimension = 2;
		}
		return dimension;
	}
	
	/**
	 * Permet d'afficher le menu pour commencer le jeu
	 */
	public static void affichageMenuScores() {
		System.out.println("Bienvenu dans cette nouvelle aventure.\n"
				+ "Vous vous aprretez à vous lancer dans une experience dont vous risquez de ne pas sortir vivant.\n"
				+ "Etes-vous pret à tenter votre chance dans le donjon Darksaroth qui fait peur de la mort qui tue ?");
		System.out.println();
		System.out.println("1 : Afficher les scores");
		System.out.println("2 : Demarrer le jeu");
		System.out.println("3 : Quitter le jeu");
		System.out.print("> ");
	}
	
	/**
	 * Permet d'afficher le demarrage du jeu
	 */
	public static void affichageDemarrageJeu(Donjon donjon) {
		
		Joueur joueur = JoueurService.trouverJoueur(donjon.getPersonnage());
		System.out.println();
		System.out.println("Aventurier à la recherche de quetes et de fortune, vous avez entendu parler du donjon Darksaroth \n"
				+ "Le Darksaroth a la reputation de ne pas avoir revu le moindre temeraire qui s'y soit aventuré.\n"
				+ "Il a aussi la reputation d'être truffé de nombreuse pièces d'or.\n"
				+ "A vous la fortune !!!\n"
				+ "Armé de votre épée vous vous approchez de l'éntrée, confiant et galvaniser par l'idée de devenir riche.\n"
				+ "Vous passez la porte d'entrée du donjon.\n"
				+ "Bienvenue dans le Darksaroth !");
		String nom = demanderNom(donjon);
		joueur.setNom((""+nom.charAt(0)).toUpperCase()+nom.substring(1));
		
		System.out.println("Vous etes au debut du donjon.");
		System.out.println();
		
		
		System.out.println(joueur.getNom()+" a " +joueur.getPointDeVie() +" points de vie, " +joueur.getForce() +" points de force et " +joueur.getPieceDOr() +" pièces d'or.");
	}
	
	/**
	 * Permet de recuperer le nom entre par le joueur
	 * @param donjon : le donjon ou evolue le joueur
	 * @return la saisie du nom du joueur
	 */
	public static String demanderNom(Donjon donjon) {
		System.out.println();
		System.out.println("Veuillez entrer votre nom :");
		System.out.print("> ");
		return SaisieGenerale.saisiePhrase(donjon.getScanner());
	}
	
	/**
	 * Permet d'afficher le menu principal du jeu.
	 * @param donjon : donjon ou evolue le joueur
	 */
	public static void menuPrincipal(Donjon donjon) {
		Personnage joueur = JoueurService.trouverJoueur(donjon.getPersonnage());
		System.out.println();
		System.out.println("Que souhaitez-vous faire : ");
		System.out.println("1 : Afficher la carte");
		System.out.println("2 : Regarder dans une direction");
		System.out.println("3 : Statistiques de "+joueur.getNom());
		if (ControlePersonnage.presenceMonstre(joueur.getCoordonnee(), donjon.getPersonnage())) {
			System.out.println("4 : Attaquer le ou les monstre(s) present(s)");
		}
		else {
			System.out.println("4 : Utiliser les objets de la salle");
			System.out.println("5 : Se deplacer");
		}
		
		System.out.println("0 : Quitter le jeu");
		System.out.print("> ");
	}
	
	/**
	 * Permet d'afficher le menu qui affiche les directions possibles
	 * @param donjon : donjon dans lequel le joueur evolue
	 * @param regarder_deplacer : true si la fonction est utilisee pour regarder, elle propose la salle ou se trouve
	 * le joueur et false si la fonction est utilisee pur se deplacer, le joueur ne reste pas au meme endroit
	 */
	public static void menuDirection(Donjon donjon, boolean regarder_deplacer) {
		Coordonnee coord = JoueurService.trouverJoueur(donjon.getPersonnage()).getCoordonnee();
		System.out.println("Dans quelle direction ? : ");
		if (regarder_deplacer) {
			System.out.println("Ici");
		}
		for (Portes porte : SalleService.directionsDiponibles(coord, donjon)) {
			System.out.println(porte.getNom());
		}
		System.out.print("> ");
	}
	
	/**
	 * Permet de demander au joueur s'il veut vraiment quitter le jeu
	 * @param donjon : donjon ou evolue le joueur
	 * @return la reponse du joueur
	 */
	public static boolean demanderQuitterLeJeu(Donjon donjon) {
		System.out.println("Voulez-vous vraiment quitter le jeu ? (oui/non) :");
		System.out.print("> ");
		return SaisieGenerale.saisieBoolean(donjon.getScanner());
	}
	
	/**
	 * Affiche que le jeu a ete perdu
	 */
	public static void perdu() {
		System.out.println("Vous avez perdu !");
	}
	
	/**
	 * Affiche que le jeu a ete gagne
	 */
	public static void gagne() {
		System.out.println("Vous avez gagne !");
	}
	
	/**
	 * Permet d'afficher la liste des scores
	 * @param listeScores : la liste des scores
	 */
	public static void affichageScores(ArrayList<Score> listeScores) {
		System.out.println();
		for (Score score : listeScores) {
			System.out.println(score.getNom()+" : "+score.getScore()+" pts a "+score.getDate());
		}
		System.out.println();
	}
	
	
	
}
