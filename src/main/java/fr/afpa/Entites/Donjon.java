package fr.afpa.Entites;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import fr.afpa.services.ObjetService;
import fr.afpa.services.PersonnageService;

public class Donjon {

	private int dimension;
	private Salle[][] plateau;
	private Scanner scanner;
	private ArrayList<Personnage> personnage;
	private ArrayList<Objet> objet;
	
	// Constructeur par defaut
	public Donjon() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	// Constructeur 
	public Donjon(int dimension, Scanner scanner) {
		super();
		this.dimension = dimension;
		this.plateau = new Salle[dimension][dimension];
			for (int i = 0; i < dimension; i++) {
				for (int j = 0; j < dimension; j++) {
					plateau[j][i]=new Salle();
				}
			}
		this.scanner = scanner;
		this.personnage = PersonnageService.creationListeDePersonnage(dimension);
		this.objet = ObjetService.creationListeDObjets(dimension);
	}

	// Getter Setter
	public int getDimension() {
		return dimension;
	}

	public Salle[][] getPlateau() {
		return plateau;
	}

	public Scanner getScanner() {
		return scanner;
	}

	public ArrayList<Personnage> getPersonnage() {
		return personnage;
	}

	public ArrayList<Objet> getObjet() {
		return objet;
	}

	public void setDimension(int dimension) {
		this.dimension = dimension;
	}

	public void setPlateau(Salle[][] plateau) {
		this.plateau = plateau;
	}

	public void setScanner(Scanner scanner) {
		this.scanner = scanner;
	}

	public void setPersonnage(ArrayList<Personnage> personnage) {
		this.personnage = personnage;
	}

	public void setObjet(ArrayList<Objet> objet) {
		this.objet = objet;
	}
	
	// hashCode
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + dimension;
		result = prime * result + ((objet == null) ? 0 : objet.hashCode());
		result = prime * result + ((personnage == null) ? 0 : personnage.hashCode());
		result = prime * result + Arrays.deepHashCode(plateau);
		result = prime * result + ((scanner == null) ? 0 : scanner.hashCode());
		return result;
	}
	
	// equals
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Donjon other = (Donjon) obj;
		if (dimension != other.dimension)
			return false;
		if (objet == null) {
			if (other.objet != null)
				return false;
		} else if (!objet.equals(other.objet))
			return false;
		if (personnage == null) {
			if (other.personnage != null)
				return false;
		} else if (!personnage.equals(other.personnage))
			return false;
		if (!Arrays.deepEquals(plateau, other.plateau))
			return false;
				return true;
	}

	//toString
	@Override
	public String toString() {
		return "Donjon [dimension=" + dimension + ", plateau=" + Arrays.toString(plateau) + ", scanner=" + scanner
				+ ", personnage=" + personnage + ", objet=" + objet + "]";
	}
	
	


	
} 
