package fr.afpa.services;

import fr.afpa.Entites.Objet;
import fr.afpa.Entites.Personnage;
import fr.afpa.Entites.PotionVie;
import fr.afpa.ihm.IHMPersonnage;

public class PotionVieService extends ObjetService {

	/**
	 * Permet d'ajouter les points de vie de la potion de vie aux points de vie du personnage
	 * @param personnage : personnage qui utilise l'objet
	 * @param objet : potion a utilise
	 * @return true si l'objet a ete utilise et false sinon
	 */
	public static boolean effetsDeLObjet(Personnage personnage, Objet objet) {
		if (objet instanceof PotionVie) {
			personnage.setPointDeVie(personnage.getPointDeVie() + objet.getValeur());
			IHMPersonnage.affichageStats(personnage, "pv");
			return true;
		}
		else {
			return false;
		}
		
	}

}
