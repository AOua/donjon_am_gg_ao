package fr.afpa.controle;

public class ControleGeneral {

	/**
	 * Verifie si le String bool passe en parametre est egal a "oui" ou "non"
	 * @param bool
	 * @return true si bool est egal a "oui" ou "non" et false sinon
	 */
	public static boolean controleBoolean(String bool) {
		return "oui".equalsIgnoreCase(bool) || "non".equalsIgnoreCase(bool);
	}
	
	/**
	 * Permet de verifier si une chaine de caractere represente un entier
	 * @param entier : la chaine de caracteres a verifier
	 * @return true si la chaine de caracteres represente un entier et false sinon
	 */
	public static boolean controleEntier(String entier) {
		try {
			Integer.parseInt(entier);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
}
