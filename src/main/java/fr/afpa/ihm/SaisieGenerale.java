package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.controle.ControleGeneral;

public class SaisieGenerale {

	/**
	 * Permet de recuperer un mot entre dans la console
	 * @param scanner : la source de la saisie
	 * @return le mot entre dans la console
	 */
	public static String saisieString(Scanner scanner) {
		String mot = scanner.next();
		scanner.nextLine();
		return mot;
	}
	
	/**
	 * Permet de recuperer un entier entre dans la console
	 * @param scanner : la source de la saisie
	 * @return l'entier entre dans la console
	 */
	public static int saisieEntier(Scanner scanner) {
		String entier = scanner.next();
		scanner.nextLine();
		if (!ControleGeneral.controleEntier(entier)) {
			System.out.println("Veuillez entrer un entier valide !");
			System.out.print("> ");
			return saisieEntier(scanner);
		}
		return Integer.parseInt(entier);
	}
	
	/**
	 * Permet de recuperer une phrase entree dans la console
	 * @param scanner : la source de la saisie
	 * @return la phrase entree dans la console
	 */
	public static String saisiePhrase(Scanner scanner) {
		return scanner.nextLine();
	}
	
	/**
	 * Permet de recuperer une reponse booleenne depuis la console
	 * @param scanner : la source de la saisie
	 * @return le booleen entre en console
	 */
	public static boolean saisieBoolean(Scanner scanner) {
		String bool = saisieString(scanner);
		if (!ControleGeneral.controleBoolean(bool)) {
			System.out.println("Veuillez choisir entre <<oui>> et <<non>> ! ");
			System.out.print("> ");
			return saisieBoolean(scanner);
		}
		else {
			return "oui".equalsIgnoreCase(bool);
		}
		
	}
	
}
