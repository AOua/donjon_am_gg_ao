package fr.afpa.Entites;

import java.util.Random;

public class BanditManchot extends Objet {

	public static final int PIECES_MAX = 50;
	
	
	public BanditManchot(Coordonnee coordonnee) {
		super("bandit manchot", new Random().nextInt(PIECES_MAX), coordonnee, "pieces d'or");
	}


	@Override
	public String toString() {
		return "BanditManchot [nom=" + nom + ", valeur=" + valeur + ", coordonnee=" + coordonnee + ", stat=" + stat
				+ ", description=" + description + "]";
	}
	
	
}
