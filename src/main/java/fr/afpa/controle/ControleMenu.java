package fr.afpa.controle;

import fr.afpa.Entites.Donjon;
import fr.afpa.Entites.Joueur;
import fr.afpa.Entites.Salle;
import fr.afpa.Entites.SalleSortie;

public class ControleMenu {

	/**
	 * Permet de verifier si la partie est perdue
	 * @param joueur : le personnage du joueur
	 * @return true si la partie est perdue et false sinon
	 */
	public static boolean perdu(Joueur joueur) {
		return !ControlePersonnage.enVie(joueur);
	}
	
	/**
	 * Permet de verifier si le joueur a gagne la partie
	 * @param joueur : le personnage du joueur
	 * @param donjon : le donjon dans lequel se trouve le joueur
	 * @return true si le joueur a gane la partie et false sinon
	 */
	public static boolean gagne(Joueur joueur, Donjon donjon) {
		Salle salle = donjon.getPlateau()[joueur.getCoordonnee().getX()][joueur.getCoordonnee().getY()];
		return ControlePersonnage.enVie(joueur) && salle instanceof SalleSortie;
	}
	
	/**
	 * Permet de verifier si la dimension est comprise entre 2 et 10 inclus
	 * @param dimension : la dimension a verifier
	 * @return true si la dimension est comprise entre 2 et 10 inclus et false sinon
	 */
	public static boolean dimensionValide(int dimension) {
		return 2<=dimension && dimension<11;
	}
	
}
