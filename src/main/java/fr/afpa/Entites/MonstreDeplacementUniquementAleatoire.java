package fr.afpa.Entites;

public class MonstreDeplacementUniquementAleatoire extends Monstre{

	
	
	public MonstreDeplacementUniquementAleatoire() {
		super();
	}

	public MonstreDeplacementUniquementAleatoire(int pointDeVie, int force, int pieceDOr, Coordonnee coordonnee,
			String description) {
		super(pointDeVie, force, pieceDOr, coordonnee, description);
		this.nom = "MonstreAléatoire"+autoIncrementationNom++;
	}

	@Override
	public String toString() {
		return "MonstreDeplacementUniquementAleatoire [nom=" + nom + ", pointDeVie=" + pointDeVie + ", force=" + force
				+ ", pieceDOr=" + pieceDOr + ", coordonnee=" + coordonnee + "]";
	}
	
	

}
