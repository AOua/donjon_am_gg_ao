package fr.afpa.services;

import java.util.ArrayList;
import java.util.Random;

import fr.afpa.Entites.Coordonnee;
import fr.afpa.Entites.Joueur;
import fr.afpa.Entites.Monstre;
import fr.afpa.Entites.MonstreDeplacementUniquementAleatoire;
import fr.afpa.Entites.MonstreDeplacementVersLeJoueur;
import fr.afpa.Entites.Personnage;
import fr.afpa.controle.ControlePersonnage;
import fr.afpa.ihm.IHMPersonnage;

public abstract class PersonnageService {

	/**
	 * Création de la liste des personnages comprenant une liste de Monstres
	 * aléatoire en fonction des dimensions du donjon et un joueur.
	 * 
	 * @param dimension longueur/largeur
	 * @return une liste de Personnage
	 */
	public static ArrayList<Personnage> creationListeDePersonnage(int dimension) {

		ArrayList<Personnage> listePersonnage = new ArrayList<Personnage>();
		listePersonnage.add(new Joueur("Héros", 50, 25, 0, new Coordonnee(0, 0)));

		for (int i = 1; i < dimension; i++) {

			int coordonneeX = new Random().nextInt(dimension);
			int coordonneeY = new Random().nextInt(dimension);

			listePersonnage.add(new Monstre(new Random().nextInt(50), new Random().nextInt(50),
					new Random().nextInt(50), new Coordonnee(coordonneeX, coordonneeY), "Immobile"));
			
			coordonneeX = new Random().nextInt(dimension);
			coordonneeY = new Random().nextInt(dimension);
			
			listePersonnage.add(new MonstreDeplacementUniquementAleatoire(new Random().nextInt(50),
					new Random().nextInt(50), new Random().nextInt(50), new Coordonnee(coordonneeX, coordonneeY),
					"Se déplace aléatoirement d'une case à une autre"));
			
			 coordonneeX = new Random().nextInt(dimension);
		 coordonneeY = new Random().nextInt(dimension);
			
			listePersonnage.add(new MonstreDeplacementVersLeJoueur(new Random().nextInt(50),
					new Random().nextInt(50), new Random().nextInt(50), new Coordonnee(coordonneeX, coordonneeY),
					"Se déplace une fois aléatoirement d'une case à une autre et une fois vers le joueur"));
		}

		return listePersonnage;
	}

	/**
	 * Déroulement d'un combat
	 * 
	 * @param joueur  Héros
	 * @param monstre
	 * @return Vrai si le combat est terminé
	 */
	public static boolean combat(Personnage joueur, Personnage monstre) {

		do {
			JoueurService.attaquer(joueur, monstre);

			if (ControlePersonnage.enVie(joueur) && ControlePersonnage.enVie(monstre)) {
				JoueurService.riposter(joueur, monstre);

			}

		} while (ControlePersonnage.enVie(monstre) && ControlePersonnage.enVie(joueur));

		if (ControlePersonnage.enVie(joueur)) {
			joueur.setPieceDOr(joueur.getPieceDOr() + monstre.getPieceDOr());
		}

		IHMPersonnage.affichageFinCombat(joueur, monstre);

		return true;

	}

	/**
	 * Déroulement des combats face à plusieurs monstres
	 * 
	 * @param joueur
	 * @param listeMonstre
	 * @return Vrai à la fin du combat
	 */
	public static boolean combatPlusieursMonstres(Personnage joueur, ArrayList<Personnage> listeMonstre) {

		ArrayList<Personnage> plusieursMonstresDansSalle = JoueurService.regarderMonstre(joueur.getCoordonnee(),
				listeMonstre);

		for (Personnage personnage : plusieursMonstresDansSalle) {

			IHMPersonnage.affichageDebutCombat(joueur, personnage);
			combat(joueur, personnage);

			if (ControlePersonnage.enVie(joueur)) {
				listeMonstre.remove(personnage);
				return true;
			}
		}

		return true;

	}

	/**
	 * Méthode qui vérifie que lors d'une attaque la cible perds ses points de vie
	 * en fonctionde la force de l'attaquant
	 * 
	 * @param cible     Personnage attaqué
	 * @param attaquant Personnage attaquant
	 * @return Vrai si la cible perd des points de vie
	 */
	public static boolean pertePointDeVie(Personnage cible, Personnage attaquant) {

		cible.setPointDeVie(cible.getPointDeVie() - attaquant.getForce());
		System.out.println(cible.getNom() + " perd " + attaquant.getForce());

		return true;
	}
}