package fr.afpa.services;

import java.util.Random;

import fr.afpa.Entites.BanditManchot;
import fr.afpa.Entites.BourseDOr;
import fr.afpa.Entites.Coordonnee;
import fr.afpa.Entites.Objet;
import fr.afpa.Entites.PotionForce;
import fr.afpa.Entites.PotionVie;

public class GenerationAleatoireService {

	private static final String SUR_TABLE = "sur une table";
	private static final String SUR_CHAISE = "sur une chaise";
	private static final String SOUS_TAPIS = "sous un tapis";
	private static final String DERRIERE_COMMODE = "derriere une commode";
	
	
	/**
	 * Permet de generer des descriptions aleatoirement
	 * @return une description
	 */
	public static String aleatoireDescriptionObjet() {
		int choix = new Random().nextInt(4);
		switch (choix) {
			case 0 : return SUR_TABLE;
			case 1 : return SUR_CHAISE;
			case 2 : return SOUS_TAPIS;
			default : return DERRIERE_COMMODE;
		}
	}
	
	/**
	 * Genere un objet aleatoire a la coordonnee entree en parametre
	 * @param coord : la coordonnee a laquelle sera creee notre objet
	 * @param banditManchot : s'il est a true, l'objet sera choisit avec la possibilite d'avoir le bandit manchot
	 * @return l'objet cree
	 */
	public static Objet aleatoireObjet(Coordonnee coord, boolean banditManchot) {
		int valeurAlea = new Random().nextInt(50);
		int choix;
		if (banditManchot) {
			choix = new Random().nextInt(4);
		}
		else {
			choix = new Random().nextInt(3);
		}
		switch (choix) {
			case 0 : return new PotionVie(valeurAlea, coord);
			case 1 : return new PotionForce(valeurAlea, coord);
			case 2 : return new BourseDOr(valeurAlea, coord);
			default : return new BanditManchot(coord);
		}
	}
	
	
}
