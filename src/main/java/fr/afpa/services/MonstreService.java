package fr.afpa.services;

import java.util.ArrayList;
import java.util.Random;


import fr.afpa.Entites.Donjon;
import fr.afpa.Entites.MonstreDeplacementUniquementAleatoire;
import fr.afpa.Entites.Personnage;
import fr.afpa.Entites.Portes;

public class MonstreService extends PersonnageService {

	public static void monstreDeplacementAleatoire(Donjon donjon) {

		for (Personnage personnage : donjon.getPersonnage()) {
			ArrayList<Portes> portesDisponibles = SalleService.directionsDiponibles(personnage.getCoordonnee(), donjon);

			if (personnage instanceof MonstreDeplacementUniquementAleatoire && !portesDisponibles.isEmpty()) {

				 int portes = new Random().nextInt(portesDisponibles.size());
						Portes porteOuverte = portesDisponibles.get(portes);
				JoueurService.actionDeplacer(personnage, porteOuverte.getDirection());

			}

		}
	}

	
	
	
	
}
