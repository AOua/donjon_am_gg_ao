package fr.afpa.tests;

import java.util.Scanner;

import fr.afpa.Entites.Donjon;
import fr.afpa.Entites.SalleSortie;
import fr.afpa.ihm.IHMAffichageDonjon;
import fr.afpa.services.DonjonService;
import fr.afpa.services.MenuService;
import fr.afpa.services.ObjetService;
import fr.afpa.services.PersonnageService;

public class TestAffichage {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		int dimension = 5;
		
		Donjon donjon = new Donjon(dimension, scanner);
		
		DonjonService.configurationDonjon(donjon);
		
	/*for (int i=0; i<donjon.getPlateau().length-1; i++) {
			for (int j=0; j<donjon.getPlateau()[0].length-1; j++) {
				donjon.getPlateau()[3][j].getPorte()[2].setPassage(true);
			}
			donjon.getPlateau()[i][0].getPorte()[1].setPassage(true);
		}
		donjon.getPlateau()[dimension-1][3] = new SalleSortie();
		
		donjon.setObjet(ObjetService.creationListeDObjets(dimension));
		donjon.setPersonnage(PersonnageService.creationListeDePersonnage(dimension));*/
		
		//IHMAffichageDonjon.affichageCarteDonjon(donjon);
		MenuService.demarrage(donjon);
		scanner.close();
	}

	
	public static void afficherNumSallesDonjon(int[][] indice) {
		for (int i = 0; i < indice.length; i++) {
			for (int j = 0; j < indice[0].length; j++) {
				System.out.print(indice[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println("\n");
	}
	
}
