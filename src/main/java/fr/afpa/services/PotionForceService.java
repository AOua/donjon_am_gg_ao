package fr.afpa.services;

import fr.afpa.Entites.Objet;
import fr.afpa.Entites.Personnage;
import fr.afpa.Entites.PotionForce;
import fr.afpa.ihm.IHMPersonnage;

public class PotionForceService extends ObjetService {
	
	/**
	 * Permet d'ajouter les points de force de la potion de force aux points de force du personnage
	 * @param personnage : personnage qui utilise l'objet
	 * @param objet : potion a utilise
	 * @return true si l'objet a ete utilise et false sinon
	 */
	public static  boolean effetsDeLObjet(Personnage personnage, Objet objet) {
		if (objet instanceof PotionForce) {
			personnage.setForce(personnage.getForce() + objet.getValeur());
			IHMPersonnage.affichageStats(personnage, "pf");
			return true;
		}
		else {
			return false;
		}
		
	}

}
