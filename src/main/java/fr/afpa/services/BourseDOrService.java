package fr.afpa.services;

import fr.afpa.Entites.BourseDOr;
import fr.afpa.Entites.Objet;
import fr.afpa.Entites.Personnage;
import fr.afpa.ihm.IHMPersonnage;

public class BourseDOrService extends ObjetService {

	/**
	 * Permet d'ajouter les pieces d'or de la bourse d'or aux pieces d'or du personnage
	 * @param personnage : personnage qui utilise l'objet
	 * @param objet : potion a utilise
	 * @return true si l'objet a ete utilise et false sinon
	 */
	public static boolean effetsDeLObjet(Personnage personnage, Objet objet) {
		if (objet instanceof BourseDOr) {
			personnage.setPieceDOr(personnage.getPieceDOr() + objet.getValeur());
			IHMPersonnage.affichageStats(personnage, "po");
			return true;
		}
		else {
			return false;
		}
		
	}

}
