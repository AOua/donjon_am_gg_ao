package fr.afpa.Entites;

public class BourseDOr extends Objet {

	public BourseDOr() {
		super();
	}

	public BourseDOr(int valeur, Coordonnee coordonnee) {
		super("bourse d'or", valeur, coordonnee, "pieces d'or");
	}

	
	@Override
	public String toString() {
		return "BourseDOr [nom=" + nom + ", valeur=" + valeur + ", coordonnee=" + coordonnee + ", stat=" + stat
				+ ", description=" + description + "]";
	}

}
