package fr.afpa.controle;

import java.util.ArrayList;

import fr.afpa.Entites.Coordonnee;
import fr.afpa.Entites.Donjon;
import fr.afpa.Entites.Joueur;
import fr.afpa.Entites.Monstre;
import fr.afpa.Entites.Personnage;
import fr.afpa.Entites.Portes;
import fr.afpa.services.JoueurService;
import fr.afpa.services.SalleService;

public class ControlePersonnage {

/**
 * Méthode qui va vérifier que les coordonnées choisies par le joueur sont disponibles	
 * @param choix
 * @param donjon
 * @return 
 */
	public static boolean controleDirectionPossible(String choix, Donjon donjon) {
		
		ArrayList<Portes> listePortesPossibles = SalleService.directionsDiponibles(JoueurService.trouverJoueur(donjon.getPersonnage()).getCoordonnee(), donjon);
		
		for (Portes porte : listePortesPossibles) {
				if((choix.toLowerCase()).equals(porte.getNom().toLowerCase())) {
					return true;
				}

		}
		
		
		return false;
		
		
		
	}
	
	
	
	/**
	 * Méthode pour vérifier si les personnages sont encore en vie après une attaque
	 * @param personnage Monstre ou Héros
	 * @return Vrai si le personnage est en vie, Faux s'il est mort
	 */
	public static boolean enVie(Personnage personnage) {

		if (personnage == null) {
			return false;
		}

		if (personnage.getPointDeVie() <= 0) {
			return false;
		}

		return true;

	}

	/**
	 * Méthode pour vérifier si un monstre est présent dans la salle
	 * @param coordonnee Position de la salle où l'on va vérifier s'il y a un monstre
	 * @param listePersonnage Qui contient les monstres
	 * @return Vrai si le monstre est présent, Faux s' il n'y en a pas
	 */

	public static boolean presenceMonstre(Coordonnee coordonnee, ArrayList<Personnage> listePersonnage) {

		if (coordonnee == null) {
			return false;
		}
		
		for (Personnage personnage : listePersonnage) {
			if (personnage instanceof Monstre && personnage.getCoordonnee().equals(coordonnee)) {
				return true;
			}
		}

		return false;

	}
	
	
	/**
	 * Methode pour vérifier si le joueur a assez de pièces d'or pour pouvoir les échanger
	 * @param personnage Joueur
	 * @param nbPiecesDOr Necessaire pour effectuer un échange
	 * @return Vrai si le personnage a assez de pièces d'or, Faux dans le cas contraire
	 */
	public static boolean assezDePieceDOr(Personnage personnage, int nbPiecesDOr) {
		
		if(personnage instanceof Joueur && personnage.getPieceDOr() >= nbPiecesDOr) {
			return true;
		}
		
		return false;
		
	}
}
