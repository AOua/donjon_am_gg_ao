package fr.afpa.Entites;

import java.util.ArrayList;
import java.util.Arrays;

public class Salle {
	
	protected int compteurMonstre;
	protected ArrayList<String> configSalle;
	protected int numero =0;
	protected Portes[]porte;
	// autoIncrementation du numero de salle pour la creation du donjon
	private static int autoIncremente = 0;
	
	//Constructeur
	public Salle() {
		super();
//		this.compteurMonstre = compteurMonstre;
//		this.configSalle = configSalle;
		this.numero = autoIncremente;
		this.porte = new Portes[4];
		porte[0] = new PorteNord();
		porte[1] = new PorteEst();
		porte[2] = new PorteSud();
		porte[3] = new PorteWest();
		autoIncremente++;
	}

	// Getter Setter
	public int getCompteurMonstre() {
		return compteurMonstre;
	}

	public ArrayList<String> getConfigSalle() {
		return configSalle;
	}

	public int getNumero() {
		return numero;
	}

	public Portes[] getPorte() {
		return porte;
	}

	public void setCompteurMonstre(int compteurMonstre) {
		this.compteurMonstre = compteurMonstre;
	}

	public void setConfigSalle(ArrayList<String> configSalle) {
		this.configSalle = configSalle;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public void setPorte(Portes[] porte) {
		this.porte = porte;
	}

	// hasCode
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + compteurMonstre;
		result = prime * result + ((configSalle == null) ? 0 : configSalle.hashCode());
		result = prime * result + numero;
		result = prime * result + Arrays.hashCode(porte);
		return result;
	}

	// equals
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Salle other = (Salle) obj;
		if (compteurMonstre != other.compteurMonstre)
			return false;
		if (configSalle == null) {
			if (other.configSalle != null)
				return false;
		} else if (!configSalle.equals(other.configSalle))
			return false;
		if (numero != other.numero)
			return false;
		if (!Arrays.equals(porte, other.porte))
			return false;
		return true;
	}

	// toString
	@Override
	public String toString() {
		return "Salle [compteurMonstre=" + compteurMonstre + ", configSalle=" + configSalle + ", numero=" + numero
				+ ", porte=" + Arrays.toString(porte) + "]";
	}
	
	
	
	
}
