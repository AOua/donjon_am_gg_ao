package fr.afpa.ihm;

import java.util.ArrayList;

import fr.afpa.Entites.Coordonnee;
import fr.afpa.Entites.Donjon;
import fr.afpa.Entites.Objet;
import fr.afpa.Entites.Personnage;
import fr.afpa.Entites.Portes;
import fr.afpa.Entites.Salle;
import fr.afpa.Entites.SalleSortie;
import fr.afpa.services.JoueurService;
import fr.afpa.services.ObjetService;

public class IHMAffichageDonjon {

	private static final String MUR_SUD = "-----+";
	private static final String PORTE_SUD = "     +";
	
	/**
	 * Permet d'afficher graphiquement le donjon
	 * @param donjon : le donjon a afficher
	 */
	public static void affichageCarteDonjon(Donjon donjon) {
		String bord = "+";
		for (int i=0; i<donjon.getPlateau()[0].length; i++) {
			bord += MUR_SUD;
		}
		System.out.println(bord);
		for (int i=0; i<donjon.getPlateau().length; i++) {
			affichageDessinDonjon(i, donjon);
		}
	}
	
	/**
	 * Permet d'echanger l'axe des x et des y pour afficher le donjon dans le bon sens
	 * @param donjon : le donjon dont on doit inverser le plateau
	 * @return une copie du plateau du donjon inversee
	 */
	private static Salle[][] plateauMiroir(Donjon donjon) {
		Salle[][] plateau = donjon.getPlateau();
		Salle[][] miroir = new Salle[plateau[0].length][plateau.length];
		for (int i=0; i<plateau.length; i++) {
			for (int j=0; j<plateau[0].length; j++) {
				miroir[j][i] = plateau[i][j];
			}
		}
		return miroir;
	}
	
	/**
	 * Permet d'afficher une ligne du plateau du donjon
	 * @param ligne : une ligne du plateau de jeu
	 * @param indiceLigne : l'indice auquel se trouve la ligne sur le plateau du donjon
	 * @param donjon : le donjon a afficher
	 */
	private static void affichageDessinDonjon(int indiceLigne, Donjon donjon) {
		for (int i=0; i<donjon.getPlateau()[indiceLigne].length; i++) {
			dessinSalle(new Coordonnee(indiceLigne, i), donjon);
		}
		for (int i=0; i<3; i++) {
			String uneLigne;
			if (i==2) {
				uneLigne = "+";
			}
			else {
				uneLigne = "|";
			}
			for (Salle salle : donjon.getPlateau()[indiceLigne]) {
				uneLigne += salle.getConfigSalle().get(i);
			}
			System.out.println(uneLigne);
		}
	}
	
	/**
	 * Permet de construire une chaine de caracteres representant le nombre d'objets
	 * presents dans une salle
	 * @param coord : les coordonnees de la salle
	 * @param listeObjets : la liste d'objets de tout le donjon
	 * @return une chaine de caracteres representant le nombre d'objets dans une salle
	 */
	private static String ligneObjets(Coordonnee coord, ArrayList<Objet> listeObjets) {
		int nbObjetsDansSalle = ObjetService.compterNombreObjetsDansSalle(coord, listeObjets);
		if (nbObjetsDansSalle > 0) {
			return String.format("%2d%s%2s", nbObjetsDansSalle, "o", "");
		}
		else {
			return String.format("%5s", "");
		}
	}
	
	/**
	 * Permet de construire une chaine de caracteres representant le nombre de monstres
	 * presents dans une salle et le heros
	 * @param coord : les coordonnees de la salle
	 * @param listePersos : la liste des personnages de tout le donjon
	 * @return une chaine de caracteres representant le nombre de monstres et le heros dans une salle
	 */
	private static String lignePerso(Coordonnee coord, ArrayList<Personnage> listePersos) {
		int nbMonstresDansSalle = JoueurService.compterNombreMonstresDansSalle(coord, listePersos);
		if (nbMonstresDansSalle > 0 && JoueurService.trouverJoueur(listePersos).getCoordonnee().equals(coord)) {
			return String.format("P/%2d%s", nbMonstresDansSalle, "M");
		}
		else if (nbMonstresDansSalle > 0 && !JoueurService.trouverJoueur(listePersos).getCoordonnee().equals(coord)) {
			return String.format("%2d%s%2s", nbMonstresDansSalle, "M", "");
		}
		else if (nbMonstresDansSalle <= 0 && JoueurService.trouverJoueur(listePersos).getCoordonnee().equals(coord)) {
			return String.format("P%4s", "");
		}
		else {
			return String.format("%5s", "");
		}
	}
	
	/**
	 * Permet de dessinner une salle
	 * @param coord : coordonnees de la salle a dessiner
	 * @param donjon : donjon auquel appartient la salle
	 */
	private static void dessinSalle(Coordonnee coord, Donjon donjon) {
		ArrayList<String> dessin = new ArrayList<String>();
		//Salle[][] tableauMiroir = plateauMiroir(donjon);
		Portes[] tabPortes = donjon.getPlateau()[coord.getX()][coord.getY()].getPorte();
		String lignePersos = lignePerso(coord, donjon.getPersonnage());
		String ligneObjet = ligneObjets(coord, donjon.getObjet());
		if (donjon.getPlateau()[coord.getX()][coord.getY()] instanceof SalleSortie) {
			if (tabPortes[1].isPassage()) {
				dessin.add("  \\/  ");
				dessin.add("  /\\  ");
			}
			else {
				dessin.add("  \\/ |");
				dessin.add("  /\\ |");
			}
			if (tabPortes[2].isPassage()) {
				dessin.add(PORTE_SUD);
			}
			else {
				dessin.add(MUR_SUD);
			}
		}
		else {
			if (tabPortes[1].isPassage()) {
				dessin.add(lignePersos+" ");
				dessin.add(ligneObjet+" ");
			}
			else {
				dessin.add(lignePersos+"|");
				dessin.add(ligneObjet+"|");
			}
			if (tabPortes[2].isPassage()) {
				dessin.add(PORTE_SUD);
			}
			else {
				dessin.add(MUR_SUD);
			}
		
		}
		donjon.getPlateau()[coord.getX()][coord.getY()].setConfigSalle(dessin);
	}
	
}
