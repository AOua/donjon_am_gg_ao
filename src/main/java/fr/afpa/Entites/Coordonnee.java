package fr.afpa.Entites;

public class Coordonnee {

	private String nom;
	private int x;
	private int y;


	public Coordonnee() {
		super();
	}
	
	

	public Coordonnee(int x, int y) {
		super();
		this.x = x;
		this.y = y;
		nom = "";
	}



	public Coordonnee(String nom, int x, int y) {
		super();
		this.nom = nom;
		this.x = x;
		this.y = y;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	@Override
	public String toString() {
		return "Coordonnee [nom=" + nom + ", x=" + x + ", y=" + y + "]";
	}
	
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coordonnee other = (Coordonnee) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
	
}
